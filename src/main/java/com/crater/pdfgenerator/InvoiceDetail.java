package com.crater.pdfgenerator;



import java.math.BigDecimal;
import java.util.List;



public class InvoiceDetail {

	//invoice properties
		private final String customerName;
		private final Long phone;
		private final String pickUpDate;
		private final String pickUpTimeSlot;
		private final BigDecimal advancePayment;	
		private final String invoiceTitle;
		private final String companyTitle;
		private final String VATTINNumber;
		private final String serviceTaxNumber;
		private final String CSTNumber;
		private final String GSTNumber;
		private final String PANNumber;
		private final String invoiceNumber;
		private final String orderNumber;
		private final String digitalSignatureHeader;
		private final List<String> termAndConditions;
		private final String invoiceFooter;
		private final Boolean isGrandTotal;
		private final Address companyAddress;
		private final Address pickupAddress;
		private final Address deliveryAddress;

		//item properties
		private final Long itemId;
		private final String productName;
		private final String brandName;
		private final String rootServiceName;
		private final String deliveryDate;
		private final BigDecimal serviceCharges;
		private final BigDecimal discount;
		private final BigDecimal igst ;
		private final BigDecimal cgst ;
		private final BigDecimal sgst ;
		private final Boolean isIntrastate;
		private final BigDecimal taxAmount;
		private final BigDecimal convinienceCharges;
		private final BigDecimal expressProcessingCharges;
		private final BigDecimal adjustment;
		private final String adjustmentType;
		private final String adjustmentSign;
		private final List<String> defects;
		
		//orderData
		private final BigDecimal grandTotal;
		private final BigDecimal totalGrossAmount;
		private final BigDecimal totalDiscount;
		private final Boolean isExpressProcessing;
		
		private final String CUSGSTNumber;
		
		@SuppressWarnings("unused")
		private InvoiceDetail() {
			this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,
					null,null,null,null,null,null, null, null,null, null, null, null, null, null, null, null, null, null, null,null, null,null, null, null,null,null,null,null,null);
		}
		
		public InvoiceDetail(
				final String customerName, 
				final Long phone, 
				final String pickUpDate, 
				final String pickUpTimeSlot,
				final BigDecimal advancePayment, 
				final String invoiceTitle, 
				final String companyTitle, 
				final String VATTINNumber, 
				final String CSTNumber, 
				final String GSTNumber, 
				final String PANNumber, 
				final String invoiceNumber, 
				final String orderNumber,
				final String digitalSignatureHeader, 
				final List<String> termAndConditions, 
				final String invoiceFooter, 
				final Long itemId, 
				final String productName,final String brandName, final String rootServiceName, 
				final String deliveryDate, final BigDecimal serviceCharges, final BigDecimal discount, final BigDecimal igst ,final BigDecimal cgst,final BigDecimal sgst,final Boolean isIntrastate,
				final BigDecimal taxAmount, final BigDecimal convinienceCharges, final Boolean isGrandTotal,
				final Address companyAddress, final Address pickupAddress, final Address deliveryAddress, final BigDecimal expressProcessingCharges, final BigDecimal grandTotal, 
				final BigDecimal totalGrossAmount, final BigDecimal totalDiscount, final Boolean isExpressProcessing, final String serviceTaxNumber,final BigDecimal adjustment,final String adjustmentType,final String adjustmentSign
				,final String CUSGSTNumber,final List<String> defects) {
			
			this.customerName=customerName;
			this.phone=phone;
			this.pickUpDate=pickUpDate;
			this.pickUpTimeSlot=pickUpTimeSlot;
			this.advancePayment=advancePayment;
			this.invoiceTitle = invoiceTitle;
			this.companyTitle = companyTitle;
			this.VATTINNumber = VATTINNumber;
			this.CSTNumber = CSTNumber;
			this.invoiceNumber = invoiceNumber;
			this.orderNumber=orderNumber;
			this.digitalSignatureHeader=digitalSignatureHeader;
			this.termAndConditions=termAndConditions;
			this.invoiceFooter=invoiceFooter;
			this.isGrandTotal=isGrandTotal;
			this.companyAddress=companyAddress;
			this.pickupAddress=pickupAddress;
			this.deliveryAddress=deliveryAddress;
			this.itemId=itemId;
			this.productName=productName;
			this.brandName=brandName;
			this.rootServiceName=rootServiceName;
			this.deliveryDate=deliveryDate;
			this.serviceCharges=serviceCharges;
			this.discount=discount;
			this.igst=igst;
			this.sgst=sgst;
			this.cgst=cgst;
			this.isIntrastate=isIntrastate;
			this.taxAmount=taxAmount;
			this.convinienceCharges=convinienceCharges;
			this.expressProcessingCharges=expressProcessingCharges;
			this.grandTotal=grandTotal;
			this.totalGrossAmount=totalGrossAmount;
			this.totalDiscount=totalDiscount;
			this.isExpressProcessing=isExpressProcessing;
			this.serviceTaxNumber=serviceTaxNumber;
			this.GSTNumber=GSTNumber;
			this.PANNumber=PANNumber;
			this.adjustment=adjustment;
			this.adjustmentType=adjustmentType;
			this.adjustmentSign=adjustmentSign;
			this.CUSGSTNumber=CUSGSTNumber;
			this.defects=defects;
		}

		
		public String getCustomerName() {
			return customerName;
		}

		public Long getPhone() {
			return phone;
		}

		public String getPickUpDate() {
			return pickUpDate;
		}

		
		public String getPickUpTimeSlot() {
			return pickUpTimeSlot;
		}

		public BigDecimal getAdvancePayment() {
			return advancePayment;
		}

		public String getInvoiceTitle() {
			return invoiceTitle;
		}

		public String getCompanyTitle() {
			return companyTitle;
		}

		public String getVATTINNumber() {
			return VATTINNumber;
		}

		public String getCSTNumber() {
			return CSTNumber;
		}

		public String getInvoiceNumber() {
			return invoiceNumber;
		}

		public String getOrderNumber() {
			return orderNumber;
		}

		public String getDigitalSignatureHeader() {
			return digitalSignatureHeader;
		}

		public List<String> getTermAndConditions() {
			return termAndConditions;
		}

		public String getInvoiceFooter() {
			return invoiceFooter;
		}

		public Boolean getIsGrandTotal() {
			return isGrandTotal;
		}

		public Address getCompanyAddress() {
			return companyAddress;
		}

		public Address getPickupAddress() {
			return pickupAddress;
		}

		public Address getDeliveryAddress() {
			return deliveryAddress;
		}

		public Long getItemId() {
			return itemId;
		}

		public String getProductName() {
			return productName;
		}

		public String getRootServiceName() {
			return rootServiceName;
		}

		public String getDeliveryDate() {
			return deliveryDate;
		}

		public BigDecimal getServiceCharges() {
			return serviceCharges;
		}

		public BigDecimal getDiscount() {
			return discount;
		}

		
		public BigDecimal getIgst() {
			return igst;
		}

		public BigDecimal getCgst() {
			return cgst;
		}

		public BigDecimal getSgst() {
			return sgst;
		}

		public Boolean getIsIntrastate() {
			return isIntrastate;
		}

		public BigDecimal getTaxAmount() {
			return taxAmount;
		}

		public BigDecimal getConvinienceCharges() {
			return convinienceCharges;
		}

		public BigDecimal getGrandTotal() {
			return grandTotal;
		}

		public BigDecimal getTotalGrossAmount() {
			return totalGrossAmount;
		}

		public BigDecimal getTotalDiscount() {
			return totalDiscount;
		}

		public BigDecimal getExpressProcessingCharges() {
			return expressProcessingCharges;
		}

		public Boolean isExpressProcessing() {
			return isExpressProcessing;
		}	
		
		public String getServiceTaxNumber() {
			return serviceTaxNumber;
		}
		
		public String getGSTNumber() {
			return GSTNumber;
		}

		public String getPANNumber() {
			return PANNumber;
		}

		
		public BigDecimal getAdjustment() {
			return adjustment;
		}

		public String getAdjustmentType() {
			return adjustmentType;
		}

		public BigDecimal getTotalTaxAmount(){
			return taxAmount;
		}

		public String getAdjustmentSign() {
			return adjustmentSign;
		}

		public String getCUSGSTNumber() {
			return CUSGSTNumber;
		}

		public String getBrandName() {
			return brandName;
		}

		public List<String> getDefects() {
			return defects;
		}
		
	}
