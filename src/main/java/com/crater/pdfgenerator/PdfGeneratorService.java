package com.crater.pdfgenerator;

import java.io.File;
import java.util.List;


public interface PdfGeneratorService {

	
	File createInvoices(List<InvoiceDetail> invoiceDetails, Boolean b,String docType, Byte workshopId, Long orderId,
			AffiliateType wedoshoes);
	

	}
