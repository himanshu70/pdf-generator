package com.crater.pdfgenerator;

public class Address {

	private final String pin;
	private final String name;
	private final String lastName;
	private final String city;
	private final String phone;
	private final String state;
	private final String country;
	private final String landmark;
	private final String streetAddress;

	@SuppressWarnings("unused")
	private Address() {
		this(null,null, null, null, null, null, null, null, null);
	}

	public Address(final String name,final String lastName, final String landmark, final String city, final String pin,  final String state, 
			final String country, final String phone, final String streetAddress){
		this.name=name;
		this.lastName=lastName;
		this.landmark=landmark;
		this.city=city;
		this.pin=pin;
		this.state=state;
		this.country=country;
		this.phone=phone;
		this.streetAddress=streetAddress;
	}

	public String getPin() {
		return pin;
	}

	public String getName() {
		return name;
	}

	public String getCity() {
		return city;
	}

	public String getPhone() {
		return phone;
	}

	public String getState() {
		return state;
	}

	public String getCountry() {
		return country;
	}

	public String getLandmark() {
		return landmark;
	}

	public String getStreetAddress() {
		return streetAddress;
	}

	public String getLastName() {
		return lastName;
	}
	
}
