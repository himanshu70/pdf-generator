package com.crater.pdfgenerator;

import static com.crater.pdfgenerator.DefaultUtils.defaultBigDecimalIfNull;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.font.encoding.WinAnsiEncoding;
import org.apache.pdfbox.pdmodel.graphics.image.JPEGFactory;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.krysalis.barcode4j.HumanReadablePlacement;
import org.krysalis.barcode4j.impl.code128.Code128Bean;
import org.krysalis.barcode4j.impl.code128.Code128Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.crater.barcodegenerator.BarcodeGenerator;
import com.crater.barcodegenerator.EncodedData;


@Service
public class PdfGeneratorServiceImpl implements PdfGeneratorService{

//	private static final Logger LOGGER = LoggerFactory.getLogger(PdfGeneratorServiceImpl.class);
	
	private final Map<String, String> wedoshoes;
	private final Map<String, String> urbanclap;
	private final Map<String, String> invoiceBarcodeConfig;
	private static final String adjstWithPosSign= "(+)";
	private static final String adjsttWithNegSign= "(-)";
	
	@Autowired
	public PdfGeneratorServiceImpl(@Value("#{wedoshoes}") final Map<String, String> wedoshoes,
			@Value("#{urbanclap}") final Map<String, String> urbanclap,
			@Value("#{invoiceBarcodeConfig}") final Map<String, String> invoiceBarcodeConfig) {
		this.wedoshoes=wedoshoes;
		this.urbanclap=urbanclap;
		this.invoiceBarcodeConfig=invoiceBarcodeConfig;
	}

	@Override
	public File createInvoices(final List<InvoiceDetail> invoiceDetails, Boolean isGrandTotal,
			final String docType,final Byte workshopId, final Long orderId,
			final AffiliateType affiliateType) {
		
		 Map<String, String> affiliate=findAffiliateDetails(affiliateType);
		 if(affiliate.isEmpty()){
			 affiliate=wedoshoes;
		 }
		PDDocument doc = new PDDocument();		 
		BigDecimal grandTotal = BigDecimal.ZERO;
		File file=null;
		final String invoiceDirectory=affiliate.get("INVOICE_DIRECTORY");
		for(InvoiceDetail invoiceDetail : invoiceDetails){
		final Byte WORKSHOP_ID=1;
			PDPageContentStream contentStream = null;	
			PDPage page = new PDPage();
			doc.addPage(page);
			try
			{
			contentStream = new PDPageContentStream(doc, page);
			PDImageXObject backgroundImage = PDImageXObject.createFromFile(affiliate.get("INVOICE_HEADER_LOGO"), doc);
//			LOGGER.info("PDI: "+backgroundImage);
			
			contentStream.drawImage(backgroundImage, 130, 250);
			
			final Code128Bean code = new Code128Bean();
			code.setCodeset(Code128Constants.CODESET_B);
			code.setFontSize(Double.parseDouble(invoiceBarcodeConfig.get("BARCODE_FONT_SIZE")));
			code.setModuleWidth(Double.parseDouble(invoiceBarcodeConfig.get("BARCODE_MODULE_WIDTH")));
			code.setBarHeight(Double.parseDouble(invoiceBarcodeConfig.get("BARCODE_HEIGHT")));
			code.setQuietZone(Double.parseDouble(invoiceBarcodeConfig.get("BARCODE_QUIET_ZONE")));
			code.setFontName(invoiceBarcodeConfig.get("BARCODE_FONT"));
			code.setMsgPosition(HumanReadablePlacement.HRP_NONE);
			
		    PDImageXObject barcodeImage = JPEGFactory.createFromImage(doc, new BarcodeGenerator(code)
				.generateBarcode(new EncodedData(workshopId,orderId, invoiceDetail.getItemId())));
		
		    contentStream.drawImage(barcodeImage, Float.parseFloat(invoiceBarcodeConfig.get("BARCODE_X_POSITION")), Float.parseFloat(invoiceBarcodeConfig.get("BARCODE_Y_POSITION")));
		
			float colWidthOfitemDetail[] = { 50, 50, 80,70, 70, 70, 50, 50, 100 };			

			if(docType.equals(DocType.INVOICE.toString())){					
				mergeOrderDataToInvoiceTemplate(contentStream, 40, 745, getHeader(invoiceDetail,affiliate)); 
			}else{
				mergeOrderDataToInvoiceTemplate(contentStream, 40, 745, getHeaderForChallan(invoiceDetail, affiliate));
			}
				mergeOrderDataToInvoiceTemplate(contentStream, 40, 730, getcompanyAddressData(invoiceDetail,affiliate));
				mergeOrderDataToInvoiceTemplate(contentStream, 40, 650, getcompanyContactData(invoiceDetail,affiliate));
				
				if(docType.equals(DocType.INVOICE.toString())){					
					mergeOrderDataToInvoiceTemplate(contentStream, 350, 660, getInvoiceNumberData(WORKSHOP_ID, invoiceDetail)); 
				}
				mergeOrderDataToInvoiceTemplate(contentStream, 40, 590, getPickupAddressData(invoiceDetail)); 
				mergeOrderDataToInvoiceTemplate(contentStream, 350, 590, getDeliveryAddressData(invoiceDetail)); 
				if (docType.equals(DocType.INVOICE.toString())) {
					mergeOrderDataToInvoiceTemplate(contentStream, 40, 510, getorderNumberData(invoiceDetail)); 					 
				}else{
					mergeOrderDataToInvoiceTemplate(contentStream, 40, 510, getorderNumberDataForChallan(invoiceDetail));
				}
				grandTotal=grandTotal.add(invoiceDetail.getTotalGrossAmount());
				if(isGrandTotal && (invoiceDetails.indexOf(invoiceDetail)==invoiceDetails.size()-1)){					
					drawItemDetailedTable(page, contentStream, 40, 500, ItemTemplateWithGrandTotal(invoiceDetail, grandTotal), colWidthOfitemDetail, invoiceDetail.isExpressProcessing());
					if(!invoiceDetail.getDefects().isEmpty()){
						createInvoiceTemplateWithDefects(contentStream,invoiceDetail,affiliate,doc);
					}else{
					mergeOrderDataToInvoiceTemplate(contentStream, 40, 270, getTermAndConditionsData(invoiceDetail,affiliate));
					mergeOrderDataToInvoiceTemplate(contentStream, 450, 272,getDigitalSignatureData(invoiceDetail,affiliate));
					mergeOrderDataToInvoiceTemplate(contentStream, 435, 192,getComputerGeneratedData(invoiceDetail,affiliate)); 

					PDImageXObject signature = PDImageXObject.createFromFile(affiliate.get("INVOICE_SIGNATURE_LOGO"), doc);
					contentStream.drawImage(signature, 450, 220);
					}
				}else{
					drawItemDetailedTable(page, contentStream, 40, 500, writeItemDataWithoutGrandTotal(invoiceDetail), colWidthOfitemDetail, invoiceDetail.isExpressProcessing());
					if(!invoiceDetail.getDefects().isEmpty()){
						createInvoiceTemplateWithDefects(contentStream,invoiceDetail,affiliate,doc);
					}else{
					mergeOrderDataToInvoiceTemplate(contentStream, 40, 270, getTermAndConditionsData(invoiceDetail,affiliate));
					mergeOrderDataToInvoiceTemplate(contentStream, 450, 272,getDigitalSignatureData(invoiceDetail,affiliate)); 
					mergeOrderDataToInvoiceTemplate(contentStream, 435, 192,getComputerGeneratedData(invoiceDetail,affiliate)); 

					PDImageXObject signature = PDImageXObject.createFromFile(affiliate.get("INVOICE_SIGNATURE_LOGO"), doc);
					contentStream.drawImage(signature, 450, 220);
					}
			}
			contentStream.close();
            clearInvoiceDirectory(invoiceDirectory,".pdf");
            if(docType.equals("CHALLAN")){
            	file= new File(invoiceDirectory+WORKSHOP_ID+"-"+invoiceDetail.getOrderNumber()+"-"+invoiceDetail.getItemId()+"_challan.pdf");
            }else{
            	file= new File(invoiceDirectory+WORKSHOP_ID+"-"+invoiceDetail.getOrderNumber()+"-"+invoiceDetail.getItemId()+"_invoice.pdf");
            }
            if((invoiceDetails.indexOf(invoiceDetail)==invoiceDetails.size()-1)){				
				doc.save(file);
				   }
			}catch(Exception e){
				e.printStackTrace();
			}
			
			
			}
		try {
			doc.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
 		return file;
	}
	
	private void createInvoiceTemplateWithDefects(final PDPageContentStream contentStream,final InvoiceDetail invoiceDetail,final Map<String, String> affiliate,final PDDocument doc) throws Exception {
		mergeOrderDataToInvoiceTemplate(contentStream, 40, 270, getDefectsData(invoiceDetail));
		final Integer defectSize=invoiceDetail.getDefects().size()+1;
		mergeOrderDataToInvoiceTemplate(contentStream, 40, 270-defectSize*10, getTermAndConditionsData(invoiceDetail,affiliate));
		mergeOrderDataToInvoiceTemplate(contentStream, 450, 272-defectSize*10,getDigitalSignatureData(invoiceDetail,affiliate));
		mergeOrderDataToInvoiceTemplate(contentStream, 435, 192-defectSize*10,getComputerGeneratedData(invoiceDetail,affiliate)); 
		PDImageXObject signature = PDImageXObject.createFromFile(affiliate.get("INVOICE_SIGNATURE_LOGO"), doc);
		contentStream.drawImage(signature, 450, 220-defectSize*10);
	}

	private String[] getDefectsData(final InvoiceDetail invoiceDetail) {
		invoiceDetail.getDefects().add(0, "Item Defects:");
		return invoiceDetail.getDefects().toArray(new String[] {});	 
	}

	private Map<String, String> findAffiliateDetails(final AffiliateType affiliateType) {
		if(affiliateType==AffiliateType.WEDOSHOES){
			return wedoshoes;
		}else if(affiliateType==AffiliateType.URBAN_CLAP){
			return urbanclap;
		}
		return Collections.emptyMap();
	}

	private static void clearInvoiceDirectory(final String invoicedirectory, final String extension) {
		final File dir = new File(invoicedirectory);
		if (dir!=null) {			 
			Arrays.stream(dir.listFiles((f, p) -> p.endsWith(extension))).forEach(File::delete);    		
		}
	}
	
	public static String[][] writeItemDataWithoutGrandTotal(final InvoiceDetail invoiceDetail) {
		String[][] ItemTemplateWithoutGrandTotal = {
				{ "ITEM \nID", "SAC CODE", "DESCRIPTION", "DELIVERY \nDATE", 
					"SERVICE\nCHARGES\n(tax exclusive)", "Express Delivery \nCharges (+)", 
					"Convenience \nCharges (+)", "DISCOUNT", "TOTAL \nAMOUNT"
					},
				
				{ "" +invoiceDetail.getItemId(), "998721",
					invoiceDetail.getBrandName() +"\n"+invoiceDetail.getProductName() + "\n(" +invoiceDetail.getRootServiceName()+ ")", 
				  invoiceDetail.getDeliveryDate(), 
				  "Rs. " +(invoiceDetail.getServiceCharges()), 
				  "Rs. " +(invoiceDetail.getExpressProcessingCharges()),  
				  "Rs. " + (invoiceDetail.getConvinienceCharges()),  
				  "Rs. "+(invoiceDetail.getDiscount()), 
				  "RS. "+ (invoiceDetail.getServiceCharges()).add((invoiceDetail.getConvinienceCharges()).add((invoiceDetail.getExpressProcessingCharges()).subtract((invoiceDetail.getDiscount()))))},  
				
				{ "", "","", 
					  "TOTAL\nGROSS AMOUNT", "TOTAL\nDISCOUNT",""+checkSign(invoiceDetail.getAdjustmentSign()), 
					  "TAX\nTYPE", /*"TAX\nRATE"*/"", "TAX\nAMOUNT\n(Incl in Gross Amt.)" },
				
				{ "", "","", 
						  "Rs. "+(invoiceDetail.getTotalGrossAmount()), !isNegSign(invoiceDetail.getAdjustmentSign()) ? 
									"Rs. "+ defaultBigDecimalIfNull(invoiceDetail.getTotalDiscount()).setScale(2, RoundingMode.HALF_UP) : 
										"Rs. "+ defaultBigDecimalIfNull(invoiceDetail.getTotalDiscount().add(defaultBigDecimalIfNull(invoiceDetail.getAdjustment()))).setScale(2, RoundingMode.HALF_UP),
						  "RS."+defaultBigDecimalIfNull(invoiceDetail.getAdjustment()),
						  invoiceDetail.getIgst().compareTo(BigDecimal.valueOf(0.00))!=0 ? "IGST @"+invoiceDetail.getIgst()+"%": "CGST @"+invoiceDetail.getCgst()+"% , \nSGST @"+invoiceDetail.getSgst()+"%", /*(invoiceDetail.getTaxPercentage())+"%"*/"", 
					"Rs. "+invoiceDetail.getTaxAmount()},
				};
		return ItemTemplateWithoutGrandTotal;
	}

	public static String[][] ItemTemplateWithGrandTotal(final InvoiceDetail invoiceDetail, final BigDecimal grandTotal) {
		String[][] itemDetailedContentWithGrandTotal = {
				{ "ITEM \nID", "SAC CODE", "DESCRIPTION", "DELIVERY \nDATE", "SERVICE\nCharges\n(tax exclusive)", "Express Delivery \nCharges (+)", "Convenience \nCharges (+)", "DISCOUNT", "TOTAL \nAMOUNT"},
				
				{ "" +invoiceDetail.getItemId(),"998721", 
				  invoiceDetail.getBrandName() +"\n"+invoiceDetail.getProductName() + "\n(" +invoiceDetail.getRootServiceName()+ ")", 
				  invoiceDetail.getDeliveryDate(), 
				  "Rs. " +defaultBigDecimalIfNull(invoiceDetail.getServiceCharges()), 
				  "Rs. " +defaultBigDecimalIfNull(invoiceDetail.getExpressProcessingCharges()),  
				  "Rs. " + defaultBigDecimalIfNull(invoiceDetail.getConvinienceCharges()),  
				  "Rs. "+defaultBigDecimalIfNull(invoiceDetail.getDiscount()), 
				  "RS. "+ defaultBigDecimalIfNull(invoiceDetail.getServiceCharges())
				  .add(defaultBigDecimalIfNull(invoiceDetail.getConvinienceCharges())
						  .add(defaultBigDecimalIfNull(invoiceDetail.getExpressProcessingCharges())
								  .subtract(defaultBigDecimalIfNull(invoiceDetail.getDiscount())))).setScale(2, RoundingMode.HALF_UP)},  
				{ "","","",  "TOTAL\nGROSS AMOUNT", "TOTAL\nDISCOUNT",""+checkSign(invoiceDetail.getAdjustmentSign()), "TAX\nTYPE", /*"TAX\nRATE",*/"", "TAX\nAMOUNT\n(Incl in Gross Amt.)" },
				{ "","","","Rs. "+defaultBigDecimalIfNull(invoiceDetail.getTotalGrossAmount()), !isNegSign(invoiceDetail.getAdjustmentSign()) ? 
					"Rs. "+ defaultBigDecimalIfNull(invoiceDetail.getTotalDiscount()).setScale(2, RoundingMode.HALF_UP) : 
						"Rs. "+ defaultBigDecimalIfNull(invoiceDetail.getTotalDiscount().add(defaultBigDecimalIfNull(invoiceDetail.getAdjustment()))).setScale(2, RoundingMode.HALF_UP),
					"RS."+defaultBigDecimalIfNull(invoiceDetail.getAdjustment()),
					 invoiceDetail.getIgst().compareTo(BigDecimal.valueOf(0.00))!=0 ? "IGST @"+invoiceDetail.getIgst()+"%": "CGST @"+invoiceDetail.getCgst()+"% , \nSGST @"+invoiceDetail.getSgst()+"%"
							 , /*defaultBigDecimalIfNull(invoiceDetail.getTaxPercentage())+"%"*/"", 
					"Rs. "+invoiceDetail.getTaxAmount()},
				{ "GRAND TOTAL","", "", "", "", "", "", "", "Rs. "+grandTotal.setScale(2,RoundingMode.HALF_UP)},  
				};
		
		return itemDetailedContentWithGrandTotal;
	}
	
	private static String checkSign(String sign) {
		if (sign.equals(adjstWithPosSign)) {
			return "(Added Service)";
		} else {
			return "Discount";
		}

	}

	private static boolean isNegSign(String sign) {
		if (sign.equals(adjstWithPosSign)) {
			return false;
		} else {
			return true;
		}

	}
	
	public static void mergeOrderDataToInvoiceTemplate(PDPageContentStream contentStream, int positionX, int positionY, String [] message) throws Exception {
		if(message !=null){
		for (int k = 0; k < message.length; k++) {
			if ( message[k] != null && !message[k].isEmpty() ) {
				final String msg=checkValidMessage(message[k]);
				contentStream.beginText();
				if (k == 0) {
					contentStream.setFont(PDType1Font.HELVETICA_BOLD, 9);
				} else {
					contentStream.setFont(PDType1Font.HELVETICA, 9);
				}
				contentStream.newLineAtOffset(positionX, positionY);
				contentStream.showText(msg);
				contentStream.endText();
				positionY = positionY - 10;
			}
		}
		 contentStream.setLineWidth((float) 0.10);
		}
	}

	private static String checkValidMessage(String msg) {
		for(int i=0;i<msg.length();i++){
			if (!WinAnsiEncoding.INSTANCE.contains(msg.charAt(i))) {
			    msg=msg.replace(msg.charAt(i), ' ');
			 }
			}
		return msg;
	}
	
	@SuppressWarnings("deprecation")
	public static void drawItemDetailedTable(PDPage page, PDPageContentStream contentStream, float margin, float y, String[][] content, float[] colWidths,final Boolean expressProcessing)
			throws IOException {
		final int rows = content.length;
		final int cols = content[0].length;
		final float rowHeight = 40f;
		final float tableWidth = page.getMediaBox().getWidth() - margin - margin;
		// final float tableHeight = rowHeight * rows;
		final float colWidth = tableWidth / cols;
		final float cellMargin = 5f;
		// draw the rows
		float nexty = y;
		for (int i = 0; i <= rows; i++) {
			if (i == 0 || i == 2 || i == 3 || i == 4 || i==5) {
				contentStream.moveTo(margin, nexty);
				contentStream.lineTo(margin + tableWidth, nexty);
				contentStream.stroke();
				contentStream.setLineWidth((float) 1.0);
			}
			nexty -= rowHeight;
		}

		contentStream.setFont(PDType1Font.HELVETICA, 8);
		float textx = margin + cellMargin;
		float texty = y - 15;
		for (int i = 0; i < content.length; i++) {

			for (int j = 0; j < content[i].length; j++) {
				if((i==1 && j==0) || (i==1 && j==1) || i==2 ||(i==6 && j==0)){
					contentStream.setFont(PDType1Font.HELVETICA_BOLD, 8);
				}else{
					contentStream.setFont(PDType1Font.HELVETICA, 8);
				}	
				String text = content[i][j];
				contentStream.beginText();
				contentStream.newLineAtOffset(textx, texty);

				if (text.contains("\n")) {
					String[] lines = text.split("\n");
					contentStream.appendRawCommands(10 + " TL\n");
					for (int k = 0; k < lines.length; k++) {
						contentStream.showText(lines[k]);
						if (k < lines.length - 1) {
							contentStream.appendRawCommands("T*\n");
						}
					}
				} else {
					contentStream.showText(text);
				}
				contentStream.endText();
				textx += (colWidths != null) ? colWidths[j] : colWidth;
			}
			texty -= rowHeight;
			textx = margin + cellMargin;
		}
	}

//	private static BigDecimal getTaxAmount(final InvoiceDetail invoiceDetail){
//		return (invoiceDetail.getServiceCharges())
//		.add((invoiceDetail.getConvinienceCharges())
//				.add((invoiceDetail.getExpressProcessingCharges())
//						.subtract((invoiceDetail.getDiscount()))));	}

 
private static String[] getComputerGeneratedData(final InvoiceDetail invoiceDetail, final Map<String, String> affiliate){
	final String[] computerGeneratedTemplateString = {affiliate.get("INVOICE_FOOTER")};
		return computerGeneratedTemplateString;
}

private static String[] getorderNumberData(final InvoiceDetail invoiceDetail){
	 final String[] orderNumberString = {"Order Number       "+invoiceDetail.getOrderNumber()};
	 return orderNumberString;
	}

private static String[] getorderNumberDataForChallan(final InvoiceDetail invoiceDetail){
	 final String[] orderNumberString = {"Order Number/Challan       "+invoiceDetail.getOrderNumber()};
	 return orderNumberString;
	}


private static String[] getDigitalSignatureData(final InvoiceDetail invoiceDetail, final Map<String, String> affiliate){
		final String[] digitalSignature = {affiliate.get("DIGITAL_SIGNATURE_HEADER")};
	 return digitalSignature;
}

private static String[] getHeader(final InvoiceDetail invoiceDetail,final  Map<String, String> affiliate){
	 final String[] header = {affiliate.get("INVOICE_TITLE")};
	 return header;
}

private static String[] getHeaderForChallan(final InvoiceDetail invoiceDetail,final  Map<String, String> affiliate){
	 final String[] header = {"Challan"};
	 return header;
}

private static String[] getcompanyContactData(final InvoiceDetail invoiceDetail,final Map<String, String> affiliate){
		final String[] companyContactData = { affiliate.get("VAT_TIN_NUMBER"), affiliate.get("CST_NUMBER"),affiliate.get("SERVICE_TAX_NUMBER"), affiliate.get("GST_NUMBER"), affiliate.get("PAN_NUMBER")/*invoiceDetail.getCompanyAddress().getPhone()*/};
	 return companyContactData;
}

private static String[] getcompanyAddressData(final InvoiceDetail invoiceDetail, final Map<String, String> affiliate){
	final String[] companyAddress = {
		     affiliate.get("COMPANY_ADDRESS_TITLE"), affiliate.get("COMPANY_ADDRESS_NAME"),
			 affiliate.get("COMPANY_ADDRESS_STREET"),affiliate.get("COMPANY_ADDRESS_LANDMARK"),
			 affiliate.get("COMPANY_ADDRESS_CITY")+" - "+ affiliate.get("COMPANY_ADDRESS_PIN"), 
			 affiliate.get("COMPANY_ADDRESS_STATE"), affiliate.get("COMPANY_ADDRESS_PHONE") /*invoiceDetail.getCompanyAddress().getCountry()*/
			 };
  return companyAddress;
}

private static String[] getTermAndConditionsData(final InvoiceDetail invoiceDetail,final  Map<String, String> affiliate){
	 if(invoiceDetail.getIgst()!=null){
		 if(invoiceDetail.getIgst()!=BigDecimal.ZERO){
			return new String [] { 
					 affiliate.get("TERM_AND_CONDITIONS_TITLE"), /*invoiceDetail.getTermAndConditions().get(1),*/
					 affiliate.get("TERM_AND_CONDITIONS_L1"),
					 affiliate.get("TERM_AND_CONDITIONS_L2"),
					 affiliate.get("TERM_AND_CONDITIONS_L3"),
					 };
		 }
		return  new String [] { 
				affiliate.get("TERM_AND_CONDITIONS_TITLE"), /*invoiceDetail.getTermAndConditions().get(1),*/
				 affiliate.get("TERM_AND_CONDITIONS_L1"),
				 affiliate.get("TERM_AND_CONDITIONS_L2"),
				 affiliate.get("TERM_AND_CONDITIONS_L3"),
				 };
		 
	 }
	return null;	 
}

private static String[] getDeliveryAddressData(final InvoiceDetail invoiceDetail){
	 
final String[] deliveryAddress = {
			"Delivery Address",
			invoiceDetail.getDeliveryAddress().getName()+" "+invoiceDetail.getPickupAddress().getLastName(),
			invoiceDetail.getDeliveryAddress().getStreetAddress(),
			invoiceDetail.getDeliveryAddress().getLandmark(),
			invoiceDetail.getDeliveryAddress().getCity()+ ","+ invoiceDetail.getDeliveryAddress().getState()+ "-"+
			invoiceDetail.getDeliveryAddress().getPin(), "Phone : "+	invoiceDetail.getDeliveryAddress().getPhone() 
			   };
return deliveryAddress;

}
private static String[] getPickupAddressData(final InvoiceDetail invoiceDetail){
	 final String[] pickupAddress = {
				"Pickup Address",
				invoiceDetail.getPickupAddress().getName()+" "+invoiceDetail.getPickupAddress().getLastName(),
				invoiceDetail.getPickupAddress().getStreetAddress(),invoiceDetail.getPickupAddress().getLandmark(),
				invoiceDetail.getPickupAddress().getCity()+ ","+ invoiceDetail.getPickupAddress().getState()+ "-"+
				invoiceDetail.getPickupAddress().getPin(), "Phone : "+ invoiceDetail.getPickupAddress().getPhone(),
				invoiceDetail.getCUSGSTNumber() != null ? ("GST Number : "+invoiceDetail.getCUSGSTNumber()) :StringUtils.EMPTY
				};		 
	 return pickupAddress;
}

/**
 * Invoice number is containing {workshopId} - {orderId} - {itemId};
 */
private static String[] getInvoiceNumberData(final Byte workshopId, final InvoiceDetail invoiceDetail){
	String[] invoiceNumber = { "Invoice Number : "+invoiceDetail.getInvoiceNumber()};
	return invoiceNumber;
}
}
